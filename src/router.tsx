import React, {lazy} from 'react';
import {createBrowserRouter} from 'react-router-dom';

import App from './App';
import BannerCreate from './pages/BannerCreate/BannerCreate';

const Homepage = lazy( () => import('./pages/Homepage/Homepage'));
const BannerEdit = lazy( () => import('./pages/BannerEdit/BannerEdit'));

export const router = createBrowserRouter([
    {
        path: '/',
        element: <App />,
        children: [
            {
                path:'/',
                element: <Homepage />
            },
            {
                path:'edit/:id',
                element: <BannerEdit />
            },
            {
                path:'create',
                element: <BannerCreate />
            }
        ],
    }
]);