import React, {Suspense} from 'react';
import ReactDOM from 'react-dom/client';
import {RouterProvider} from 'react-router';
import {QueryClient, QueryClientProvider} from '@tanstack/react-query';
import {CircularProgress} from '@mui/material';
import {ReactQueryDevtools} from '@tanstack/react-query-devtools';

import './index.css';
import {router} from './router';
import BannerContextProvider from './context/bannerContext';
import {ScrollProvider} from './context/scrollContext';

const queryClient = new QueryClient();

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <ReactQueryDevtools initialIsOpen={false}/>
            <ScrollProvider>
                <BannerContextProvider>
                    <Suspense fallback={<CircularProgress/>}>
                        <RouterProvider router={router}/>
                    </Suspense>
                </BannerContextProvider>
            </ScrollProvider>
        </QueryClientProvider>
    </React.StrictMode>,
);
