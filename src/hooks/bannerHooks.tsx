import { useMutation, useQuery, useQueryClient} from '@tanstack/react-query';

import {
    createBanner,
    deleteBanner,
    getBannerDetail,
    getBanners,
    getBannersWithPagination,
    updateBanner
} from '../apis/bannerApi';

export const useGetBanners = (page: number, limit: number, order: string, sortBy: string) => {
    return useQuery(
        ['bannersPaginateData', page, limit, order, sortBy],
        () => getBannersWithPagination(page, limit, order, sortBy), {
            refetchOnWindowFocus: false,
            staleTime: 60000
        });
};

export const useGetAllBanners = () => {
    return useQuery(['bannersData'],
        () => getBanners(),
        {
            refetchOnWindowFocus: false,
            staleTime: 60000
        });
};
export const useGetBannerById = (id?: string, enabled?: boolean) => {
    return useQuery(['getBanner', id], () => {
            try {
                if (id) {
                    return getBannerDetail(id);
                }
            } catch (e) {
                return Promise.reject(new Error('L identifiant n\'est pas valable'));
            }
            return Promise.reject(new Error('L identifiant n\'est pas valable'));
        },
        {
            enabled,
            refetchOnWindowFocus: false,
            staleTime: 60000,
        });
};
export const useUpdateBannerMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(updateBanner, {
        onSuccess: () => {
            queryClient.invalidateQueries();

        }
    });
};
export const useCreateBannerMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(createBanner, {
        onSuccess: () => {
            queryClient.invalidateQueries();
        }
    });
};
export const useDeleteBannerMutation = () => {
    const queryClient = useQueryClient();
    return useMutation(deleteBanner, {
        onSuccess: () => {
            queryClient.invalidateQueries();
        }
    });
};