import React from 'react';
import {Grid} from '@mui/material';

import Form from '../../components/Form';
import {useCreateBannerMutation} from '../../hooks/bannerHooks';
import {BannerInterface} from '../../interfaces/BannerInterface';

const BannerCreate : React.FC = () => {

    const createMutation = useCreateBannerMutation();
    const initialValue = {
        lastModified: '',
        createdBy: {
            firstName: '',
            lastName: ''
        },
        contributors: [],
        enabled: false,
        title: '',
        description: '',
        content: '',
        formats: [],
    };

    const handleSave = (banner: BannerInterface) => {
        // eslint-disable-next-line no-console
        console.log(banner);
        createMutation.mutate(banner);
    };

    return (
        <Grid container style={{marginTop: 16, marginBottom: 16}} spacing={3}>
            <Form
                handleSave={handleSave}
                initialValue={initialValue}
                createMode={true}
                statusSave={{
                    isLoading: createMutation.isLoading,
                    isError: createMutation.isError,
                    isSuccess: createMutation.isSuccess
                }}
            />
        </Grid>
    );
};

export default BannerCreate;