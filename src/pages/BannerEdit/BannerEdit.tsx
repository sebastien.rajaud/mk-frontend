import React, {useContext} from 'react';
import {CircularProgress, Grid} from '@mui/material';
import {Navigate, useParams} from 'react-router';

import Form from '../../components/Form';
import {BannerInterface} from '../../interfaces/BannerInterface';
import AlertSnackbar from '../../components/AlertSnackbar';
import {BannerContext} from '../../context/bannerContext';
import {useDeleteBannerMutation, useGetBannerById, useUpdateBannerMutation} from '../../hooks/bannerHooks';

const BannerEdit : React.FC = () => {
    const {id} = useParams();
    const {setSelectedBanner} = useContext(BannerContext);
    const {data, isError, error, isLoading} = useGetBannerById(id);

    const updateMutation = useUpdateBannerMutation();
    const deleteMutation = useDeleteBannerMutation();

    const handleUpdate = (banner: BannerInterface) => {
        if (id) {
            updateMutation.mutate({id, banner});
            setSelectedBanner(null);
        }
    };
    const handleDelete = (): void => {
        if (id) {
            deleteMutation.mutate(id);
            setSelectedBanner(null);
        }
    };

    if (deleteMutation.isSuccess) {
        return <Navigate to="/"/>;
    }

    return (
        <>
            {isError && <AlertSnackbar open={isError} severity="error">{error as string}</AlertSnackbar>}
            {isLoading && <CircularProgress/>}
            <Grid container style={{marginTop: 16, marginBottom: 16}} spacing={3}>
                {data && <Form
                    handleSave={handleUpdate}
                    initialValue={data}
                    handleDelete={handleDelete}
                    statusDelete={{
                        isLoading: deleteMutation.isLoading,
                        isError: deleteMutation.isError,
                        isSuccess: deleteMutation.isSuccess
                    }}
                    statusSave={{
                        isLoading: updateMutation.isLoading,
                        isError: updateMutation.isError,
                        isSuccess: updateMutation.isSuccess
                    }}
                />
                }
            </Grid>
        </>
    );
};

export default BannerEdit;