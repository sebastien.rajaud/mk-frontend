import React from 'react';
import {Link} from 'react-router-dom';
import {Button, Grid, Typography} from '@mui/material';

const ErrorPage = () => {
    return (
        <Grid container style={{marginTop: 16, marginBottom: 16}} spacing={3}>
            <Grid item xs={3}/>
            <Grid item xs={6} textAlign='center'>
                <img src="/404.png" style={{height: '400px', margin: '0 auto 40px'}} alt="Page non trouvée"/>
                <Typography variant="h4">Oooops ! La page demandée n&apos;existe pas.</Typography>
                <Button color="primary" variant="contained" component={Link}
                        to="/">Retour à l&apos;accueil</Button>
            </Grid>
            <Grid item xs={3}/>
        </Grid>
    );
};

export default ErrorPage;