import React from 'react';
import {Chip} from '@mui/material';

import {BannerChipFormatInterface} from '../../interfaces/BannerChipFormatInterface';

const BannerChipFormat: React.FC<BannerChipFormatInterface> = ({format, color = 'default', handleClick}) => {
    const string = `${format.width}x${format.height}`;
    if (!format.width || !format.height) return null;
    return <Chip color={color} style={{marginRight: 8}} label={string} onClick={handleClick}/>;
};


export default BannerChipFormat;