import React from 'react';
import {render} from '@testing-library/react';

import BannerAvatar from './BannerAvatar';
import {UserInterface} from '../../interfaces/BannerInterface';

describe('BannerAvatar component', () => {
    it('renders component without crash', () => {
        const contributor : UserInterface = {
            id: '1',
            firstName: 'Jean',
            lastName: 'Dupont'
        };
        const {getByText} = render(<BannerAvatar contributor={contributor}/>);
        const avatar = getByText('JD');
        expect(avatar).toBeInTheDocument();
    });
});