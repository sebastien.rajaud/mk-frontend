import React from 'react';
import {Avatar} from '@mui/material';

import {BannerAvatarInterface} from '../../interfaces/BannerAvatarInterface';

const BannerAvatar : React.FC<BannerAvatarInterface> = ({contributor}) => {
    const {lastName, firstName} = contributor;
    return (
        <Avatar key={contributor.id} style={{marginLeft: -16}}>
            {`${firstName[0]}${lastName[0]}`}
        </Avatar>
    );
};

export default BannerAvatar;