import React, {useContext, useEffect, useRef} from 'react';
import {Link} from 'react-router-dom';
import {Grid, IconButton, List, ListItem, ListItemIcon, ListItemText, Paper, Typography} from '@mui/material';
import {Person} from '@mui/icons-material';
import EditIcon from '@mui/icons-material/Edit';

import {ScrollContext} from '../../context/scrollContext';
import {BannerContext} from '../../context/bannerContext';
import {BannerDetailInterface} from '../../interfaces/BannerDetailInterface';

const BannerDetail: React.FC<BannerDetailInterface> = ({bannerDetail}) => {
    const {description, content, contributors, title, createdBy, lastModified} = bannerDetail;
    const {setScrollRef, scrollRef} = useContext(ScrollContext);
    const {selectedBanner} = useContext(BannerContext);
    const ref = useRef<HTMLDivElement>(null);

    const formatDate = (date: string): string => {
        const dateFormatted = new Date(date);
        return new Intl.DateTimeFormat('fr', {
            day: 'numeric',
            month: 'long',
            year: 'numeric'
        }).format(dateFormatted);
    };

    useEffect(() => {
        if (selectedBanner) {
            setScrollRef(ref);
        }
    }, [selectedBanner, setScrollRef]);
    useEffect(() => {
        scrollRef?.current?.scrollIntoView({behavior: 'smooth'});
    }, [scrollRef]);

    return (
        <Paper ref={ref} style={{padding: 16}} elevation={8}>
            <Grid container spacing={3}>
                <Grid item xs={8}>
                    <Typography variant="h6" paragraph>
                        {title}
                    </Typography>
                    <Typography paragraph>{description}</Typography>
                    <Typography paragraph>{content}</Typography>
                </Grid>
                <Grid item xs={4}>
                    <Paper elevation={0} style={{padding: 16}}>
                        <Typography paragraph variant="subtitle2">
                            Créé par {`${createdBy.firstName} ${createdBy.lastName}`}
                        </Typography>
                        <Typography paragraph variant="subtitle2">
                            Dernière modification le {formatDate(lastModified)}
                        </Typography>
                    </Paper>
                    {contributors.length > 0 && (
                        <Paper elevation={2}>
                            <List>
                                {contributors.map(contributor => (
                                    <ListItem key={contributor.id}>
                                        <ListItemIcon>
                                            <Person/>
                                        </ListItemIcon>
                                        <ListItemText primary={`${contributor.firstName} ${contributor.lastName}`}/>
                                    </ListItem>
                                ))}
                            </List>
                        </Paper>
                    )}

                </Grid>
            </Grid>
            <IconButton component={Link} to={`edit/${bannerDetail.id}`}><EditIcon/></IconButton>
        </Paper>
    );
};

export default BannerDetail;