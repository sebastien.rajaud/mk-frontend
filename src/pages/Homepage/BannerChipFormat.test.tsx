import React from 'react';
import { render} from '@testing-library/react';

import {FormatInterface} from '../../interfaces/BannerInterface';
import BannerChipFormat from './BannerChipFormat';

describe('BannerChipFormat component', () => {
    it('renders component without crash', () => {
        const format: FormatInterface = {
            width: 400,
            height: 200
        };
        const {getByText} = render(<BannerChipFormat format={format}/>);
        const bannerChipText = getByText('400x200');
        expect(bannerChipText).toBeInTheDocument();
    });

    it('not renders component without height', () => {
        const format: FormatInterface = {
            width: 400,
            height: undefined
        };
        const {queryByText} = render(<BannerChipFormat format={format}/>);
        const bannerChipText = queryByText('400xundefined');
        expect(bannerChipText).toBeNull();
    });
});