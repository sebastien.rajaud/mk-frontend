import React from 'react';
import {Grid} from '@mui/material';

import Filter from '../../components/Filter';
import {BannerFiltersInterface} from '../../interfaces/BannerFiltersInterface';

const BannerFilters : React.FC<BannerFiltersInterface>= ({
                           handleOrder,
                           order,
                           limit,
                           sort,
                           handleSort,
                           handleLimit,
                           limitValues,
                           orderValues,
                           sortValues
                       }) => {
    return (
        <Grid container spacing={3} sx={{my: '1rem'}}>
            <Grid item sm={3}></Grid>
            <Grid item sm={3} >
                <Filter inputLabel="Ordre"
                        label="Ordre"
                        onChange={handleOrder}
                        value={order}
                        values={orderValues}/>

            </Grid>
            <Grid item sm={3}>
                <Filter inputLabel="Résultats"
                        label="Résultats"
                        onChange={handleLimit}
                        value={limit.toString()}
                        values={limitValues}/>

            </Grid>

            <Grid item sm={3}>
                <Filter inputLabel="Tri"
                        label="Tri"
                        onChange={handleSort}
                        value={sort}
                        values={sortValues}/>

            </Grid>

        </Grid>
    );
};

export default BannerFilters;