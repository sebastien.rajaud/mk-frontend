import React, {useContext, useEffect, useState} from 'react';
import {
    Grid,
    Pagination,
    Paper,
    LinearProgress, SelectChangeEvent
} from '@mui/material';

import {BannerInterface} from '../../interfaces/BannerInterface';
import BannerList from './BannerList';
import BannerDetail from './BannerDetail';
import {BannerContext} from '../../context/bannerContext';
import AlertSnackbar from '../../components/AlertSnackbar';
import {useGetAllBanners, useGetBanners} from '../../hooks/bannerHooks';
import BannerFilters from './BannerFilters';

const limitValues = [
    {label: '5', value: 5},
    {label: '10', value: 10},
    {label: '15', value: 15}
];
const sortValues = [
    {label: 'Titre', value: 'title'},
    {label: 'Dernière modification', value: 'lastModified'}
];
const orderValues = [
    {label: 'ASC', value: 'asc'},
    {label: 'DESC', value: 'desc'}
];

const Homepage = () => {
    const {selectedBanner, selectedBannerState} = useContext(BannerContext);
    const [page, setPage] = useState<number>(1);
    const [limit, setLimit] = useState<number>(5);
    const [order, setOrder] = useState<string>('desc');
    const [sort, setSort] = useState<string>('lastModified');
    const [countPages, setCountPages] = useState<number>(1);

    const {isLoading, isError, error, data} = useGetBanners(page, limit, order, sort);
    const {data: allBanners} = useGetAllBanners();

    const handlePages = (e: React.ChangeEvent<unknown>, page: number): void => {
        setPage(page);
    };
    const handleLimit = (e: SelectChangeEvent) => {
        setLimit(parseInt(e.target.value));
    };
    const handleOrder = (e: SelectChangeEvent) => {
        setOrder(e.target.value);
    };

    const handleSort = (e: SelectChangeEvent) => {
        setSort(e.target.value);
    };

    useEffect(() => {
        if (allBanners) {
            setCountPages(() => Math.ceil(allBanners.length / limit));
        }
    }, [limit, allBanners]);


    if (isLoading) {
        return <LinearProgress color="secondary"/>;
    }

    const creatives: BannerInterface[] = data || [];

    return (
        <>
            {isError && <AlertSnackbar open={isError} severity="error">{error as string}</AlertSnackbar>}
            <Grid container style={{marginTop: 16, marginBottom: 16}} spacing={3}>
                <Grid item xs={2}/>
                <Grid item xs={8}>

                </Grid>
                <Grid item xs={2}/>

                <Grid item xs={2}/>
                <Grid item xs={8}>
                    <Paper style={{padding: 16}} elevation={8}>
                        <BannerFilters order={order}
                                       handleOrder={handleOrder}
                                       limit={limit}
                                       sort={sort}
                                       handleLimit={handleLimit}
                                       handleSort={handleSort}
                                       orderValues={orderValues}
                                       limitValues={limitValues}
                                       sortValues={sortValues}
                        />
                        <BannerList creatives={creatives}/>
                    </Paper>
                </Grid>

                <Grid item xs={12}>
                    <Grid container justifyContent="center">
                        <Grid item>
                            <Pagination count={countPages} page={page} onChange={handlePages}/>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={2}/>
                <Grid item xs={8}>
                    {(selectedBannerState.isFetching) && <LinearProgress/>}
                    {selectedBanner && !selectedBannerState.isLoading && !selectedBannerState.isFetching &&
                        <BannerDetail bannerDetail={selectedBanner}/>}
                </Grid>


            </Grid>
        </>
    );
};

export default Homepage;