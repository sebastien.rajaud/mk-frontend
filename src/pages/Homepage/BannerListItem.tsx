import React, {useContext, useEffect} from 'react';
import {Grid, ListItem, ListItemText, Switch, Typography, CircularProgress} from '@mui/material';

import BannerChipFormat from './BannerChipFormat';
import BannerAvatar from './BannerAvatar';
import {BannerContext, BannerContextInterface} from '../../context/bannerContext';
import AlertSnackbar from '../../components/AlertSnackbar';
import {useGetBannerById, useUpdateBannerMutation} from '../../hooks/bannerHooks';
import {BannerListItemInterface} from '../../interfaces/BannerListItemInterface';

const BannerListItem: React.FC<BannerListItemInterface> = ({creative, divider}) => {
    const {
        setSelectedBanner,
        setSelectedBannerState,
        selectedBanner
    } = useContext<BannerContextInterface>(BannerContext);

    const {refetch, isLoading, error, isError, isFetching} = useGetBannerById(creative.id, false);
    const {mutate, isSuccess: isCheckSuccess, isLoading: isCheckLoading} = useUpdateBannerMutation();

    const handleClick = async (): Promise<void> => {
        const {data} = await refetch();
        setSelectedBanner(data);
    };

    const handleCheck = (): void => {
        const banner = {
            ...creative,
            enabled: !creative.enabled
        };
        if (creative.id) {
            mutate({id: creative.id, banner});
        }
    };

    useEffect(() => {
        setSelectedBannerState({isLoading, error, isFetching});
    }, [isLoading, error, isFetching, setSelectedBannerState]);

    return (
        <>
            {isError && <AlertSnackbar severity="error" open={isError}>Il y a une erreur.</AlertSnackbar>}
            <AlertSnackbar severity="success" open={isCheckSuccess}>La bannière est mise à jour.</AlertSnackbar>
            <ListItem
                key={creative.formats.join('-')}
                secondaryAction={(isCheckLoading) ? <CircularProgress/> :
                    <Switch checked={creative.enabled} onChange={handleCheck}/>}
                divider={divider} data-testid="toggleListItem"
            >
                <ListItemText
                    primary={
                        <Grid container spacing={3}>
                            <Grid item xs={3}>
                                <Typography
                                    variant="h6"
                                    style={{
                                        fontWeight: selectedBanner && selectedBanner.id === creative.id ?
                                            'bold' : ''
                                    }}
                                    data-testid="bannerTitle"
                                    onClick={handleClick}
                                    title="Voir en détail"
                                    sx={{cursor: 'pointer'}}
                                >
                                    {creative.title}
                                </Typography>
                            </Grid>
                            <Grid item xs={3}>
                                <div style={{display: 'flex'}}>
                                    {creative.contributors.map(contributor => <BannerAvatar
                                        contributor={contributor}
                                        key={contributor.id}/>)}
                                </div>
                            </Grid>
                            <Grid item xs={6}>
                                {creative.formats.map((format, index) => <BannerChipFormat key={index}
                                                                                           format={format}/>)}
                            </Grid>
                        </Grid>
                    }
                />
            </ListItem>


        </>
    );
};

export default BannerListItem;