import React from 'react';
import {render} from '@testing-library/react';
import {QueryClient, QueryClientProvider} from '@tanstack/react-query';


import BannerListItem from './BannerListItem';

describe('BannerListItem component', () => {
    const queryClient = new QueryClient();
    const banner = {
        createdBy: {
            firstName: 'Marcel',
            lastName: 'Patoulacci'
        },
        contributors: [],
        lastModified: new Date().toString(),
        enabled: false,
        title: 'A fortiori',
        description: 'Agent de la paix avant tout',
        content: '',
        formats: [],
    };

    it('renders without crash', () => {
        const {container} = render(<QueryClientProvider client={queryClient}>
            <BannerListItem creative={banner} divider={false}/>
        </QueryClientProvider>);
        expect(container).toBeInTheDocument();

    });
});