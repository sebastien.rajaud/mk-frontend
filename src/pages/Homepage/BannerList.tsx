import React from 'react';
import {List} from '@mui/material';

import BannerListItem from './BannerListItem';
import {BannerListInterface} from '../../interfaces/BannerListInterface';

const BannerList : React.FC<BannerListInterface> = ({creatives}) => {
    return (
        <List>
            {creatives.map((creative, index) => (
                <BannerListItem key={index} divider={index < creatives.length - 1}
                                creative={creative} />
            ))}
        </List>
    );
};

export default BannerList;