import React, {createContext, PropsWithChildren, useState} from 'react';

import {BannerInterface} from '../interfaces/BannerInterface';

interface SelectedBannerStateInterface {
    error: unknown | null,
    isLoading: boolean,
    isFetching: boolean
}

export interface BannerContextInterface {
    selectedBanner: BannerInterface | null,
    setSelectedBanner: (banner: BannerInterface|null) => void
    setSelectedBannerState: (selectedBannerState: SelectedBannerStateInterface) => void
    selectedBannerState: SelectedBannerStateInterface
}

export const BannerContext = createContext<BannerContextInterface>({
    selectedBanner: null,
    setSelectedBanner: (banner: BannerInterface|null) => {
        banner;
    },
    selectedBannerState: {
        error: null,
        isLoading: false,
        isFetching: false
    },
    setSelectedBannerState: (selectedBannerState: SelectedBannerStateInterface) => {
        selectedBannerState;
    }
});

const BannerContextProvider = ({children}: PropsWithChildren) => {
    const [selectedBanner, setSelectedBanner] = useState<BannerInterface | null>(null);
    const [selectedBannerState, setSelectedBannerState] = useState<SelectedBannerStateInterface>({
        error: null,
        isLoading: false,
        isFetching: false
    });

    return (
        <BannerContext.Provider value={{
            selectedBanner, setSelectedBanner,
            selectedBannerState,
            setSelectedBannerState
        }}>
            {children}
        </BannerContext.Provider>
    );
};

export default BannerContextProvider;