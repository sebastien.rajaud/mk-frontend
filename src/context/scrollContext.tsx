import React, {createContext, RefObject, useState} from 'react';

export interface ScrollContextInterface {
    scrollRef: React.RefObject<HTMLDivElement> | null,
    setScrollRef: (ref: RefObject<HTMLDivElement>) => void
}

export const ScrollContext = createContext<ScrollContextInterface>({
    scrollRef: null,
    setScrollRef: (ref) => {
        // eslint-disable-next-line no-console
        console.log(ref);
    }
});

interface ScrollProviderInterface {
    children: React.ReactNode;
}

export const ScrollProvider: React.FC<ScrollProviderInterface> = ({children}) => {
    const [scrollRef, setScrollRef] = useState<RefObject<HTMLDivElement> | null>(null);

    return (
        <ScrollContext.Provider value={{scrollRef, setScrollRef}}>
            {children}
        </ScrollContext.Provider>
    );
};