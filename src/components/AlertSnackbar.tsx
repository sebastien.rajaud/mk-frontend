import React, {useState} from 'react';
import {Alert, Snackbar} from '@mui/material';

import {AlertSnackbarInterface} from '../interfaces/AlertSnackbarInterface';


const AlertSnackbar: React.FC<AlertSnackbarInterface> = ({severity, open, children}) => {
    const [isOpen, setIsOpen] = useState<boolean>(open);
    const handleClose = () => {
        setIsOpen(false);
    };

    return (
        <Snackbar
            open={isOpen}
            autoHideDuration={6000}
            onClose={handleClose}
        >
            <Alert severity={severity}>
                {children}
            </Alert>
        </Snackbar>

    );
};

export default AlertSnackbar;