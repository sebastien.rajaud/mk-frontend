import React from 'react';
import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';

import Form from './Form';

describe('Form component', () => {
    const handleSave = vi.fn();
    const handleDelete = vi.fn();
    const statusDelete = {
        isLoading: false,
        isError: false,
        isSuccess: false,
    };
    const statusSave = {
        isLoading: false,
        isError: false,
        isSuccess: false,
    };
    const createMode = true;
    const initialValue = {
        lastModified: '',
        createdBy: {
            firstName: '',
            lastName: ''
        },
        contributors: [],
        enabled: false,
        title: '',
        description: '',
        content: '',
        formats: [],
    };

    it('render component without crash', () => {
        const {container} = render(<BrowserRouter><Form
            handleSave={handleSave}
            initialValue={initialValue}
            handleDelete={handleDelete}
            statusDelete={statusDelete}
            statusSave={statusSave}
            createMode={createMode}
        /></BrowserRouter>);
        expect(container).toBeInTheDocument();
    });

    it('render author fields in create mode', () => {
        const {getByTestId} = render(<BrowserRouter><Form
            handleSave={handleSave}
            initialValue={initialValue}
            handleDelete={handleDelete}
            statusDelete={statusDelete}
            statusSave={statusSave}
            createMode={createMode}
        /></BrowserRouter>);
        const firstNameField = getByTestId('authorFirstName');
        const lastNameField = getByTestId('authorLastName');
        expect(firstNameField).toBeInTheDocument();
        expect(lastNameField).toBeInTheDocument();
    });

    it('Should not display formatForm on load',() => {
        const {queryByTestId} = render(<BrowserRouter><Form
            handleSave={handleSave}
            initialValue={initialValue}
            handleDelete={handleDelete}
            statusDelete={statusDelete}
            statusSave={statusSave}
            createMode={false}
        /></BrowserRouter>);
        const formatForm = queryByTestId('formatForm');
        expect(formatForm).not.toBeInTheDocument();
    });

});
