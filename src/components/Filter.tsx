import React from 'react';
import {FormControl, InputLabel, MenuItem, Select} from '@mui/material';

import {FilterInterface} from '../interfaces/FilterInterface';

const Filter : React.FC<FilterInterface> = ({inputLabel, label, onChange, value, values}) => {
    return (
        <FormControl fullWidth>
            <InputLabel>{inputLabel}</InputLabel>
            <Select
                size="small"
                label={label}
                onChange={onChange}
                value={value}
            >
                {values.map((v, index) => <MenuItem key={index} value={v.value}>{v.label}</MenuItem>)}
            </Select>
        </FormControl>
    );
};

export default Filter;