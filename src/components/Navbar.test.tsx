import React from 'react';
import {render} from '@testing-library/react';
import {BrowserRouter} from 'react-router-dom';

import Navbar from './Navbar';

describe('Navbar component', () => {
    it('renders component without crash', () => {
        const {container} = render(<BrowserRouter><Navbar/></BrowserRouter>);
        expect(container).toBeInTheDocument();
    });
    it('renders logo in Navbar', () => {
        const {getByAltText} = render(<BrowserRouter><Navbar/></BrowserRouter>);
        const logo = getByAltText(/logo/i);
        expect(logo).toBeInTheDocument();
    });
    it('renders button "Créer une bannière"', () => {
        const {getByText} = render(<BrowserRouter><Navbar/></BrowserRouter>);
        const button = getByText('Créer une bannière');
        expect(button).toBeInTheDocument();
    });
});