import React, {ChangeEvent, useState} from 'react';
import {Box, Button, Grid, IconButton, Paper, Switch, TextField, Typography} from '@mui/material';
import {Add} from '@mui/icons-material';
import {useNavigate} from 'react-router';
import {Link} from 'react-router-dom';

import {BannerInterface, FormatInterface} from '../interfaces/BannerInterface';
import BannerChipFormat from '../pages/Homepage/BannerChipFormat';
import FormatForm from './FormatForm';
import AlertSnackbar from './AlertSnackbar';
import {FormInterface} from '../interfaces/FormInterface';


const Form: React.FC<FormInterface> = ({
                                           handleSave,
                                           initialValue,
                                           handleDelete,
                                           statusDelete,
                                           statusSave,
                                           createMode = false
                                       }) => {

    const [formData, setFormData] = useState<BannerInterface>(initialValue);
    const [toggleFormatForm, setToggleFormatForm] = useState<boolean>(false);
    const navigate = useNavigate();

    const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setFormData(prevState => ({
            ...prevState,
            [e.target.name]: e.target.value
        }));
    };
    const handleCreatedByChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setFormData(prevState => ({
            ...prevState,
            createdBy: {
                ...prevState.createdBy,
                [e.target.name]: e.target.value
            }
        }));
    };
    const handleSwitch = () => {
        setFormData(prevState => ({
            ...prevState,
            enabled: !prevState.enabled
        }));
    };
    const handleFormatFormDisplay = (): void => {
        setToggleFormatForm(!toggleFormatForm);
    };
    const addFormatToState = (values: FormatInterface) => {
        setFormData(prevState => ({
            ...prevState,
            formats: [
                ...prevState.formats,
                values
            ]
        }));
    };
    const handleRemoveChip = (index: number): void => {
        setFormData(prevState => ({
            ...prevState,
            formats: prevState.formats.filter((f, i) => i !== index)
        }));
    };
    const handleCancel = (): void => {
        navigate('/');
    };
    const saveData = () => {
        const newData = {
            ...formData,
            lastModified: new Date().toISOString(),
        };
        handleSave(newData);
    };

    return (
        <>
            <Grid item xs={3}/>
            <Grid item xs={6}>
                <Paper elevation={8} style={{padding: 16}}>
                    <Grid container alignItems="center">
                        <Grid item xs={8}>
                            <TextField name="title" margin="normal" label="Titre" onChange={handleChange}
                                       value={formData.title}/>
                        </Grid>
                        <Grid item xs container justifyContent="flex-end">
                            <Grid item>
                                <Switch name="enabled" onChange={handleSwitch} checked={formData.enabled}/>
                            </Grid>
                        </Grid>
                    </Grid>
                    {createMode && (
                        <Box style={{border: '1px solid lightgray', borderRadius: '5px', padding: '1em'}}>
                            <Typography variant="h6" color="primary">Auteur</Typography>
                            <Grid container alignItems="center" spacing={3}>
                                <Grid item xs={6}>
                                    <TextField data-testid="authorFirstName" fullWidth
                                               name="firstName" margin="normal" label="Prénom"
                                               onChange={handleCreatedByChange}
                                               value={formData.createdBy.firstName}/>
                                </Grid>

                                <Grid item xs={6}>
                                    <TextField data-testid="authorLastName" fullWidth
                                               name="lastName" margin="normal" label="Nom"
                                               onChange={handleCreatedByChange}
                                               value={formData.createdBy.lastName}/>
                                </Grid>

                            </Grid>
                        </Box>
                    )}


                    <TextField
                        margin="normal"
                        fullWidth
                        multiline
                        minRows={3}
                        onChange={handleChange}
                        name="description"
                        label="Description"
                        value={formData.description}
                    />
                    <TextField onChange={handleChange} name="content" margin="normal" fullWidth multiline minRows={10}
                               label="Contenu" value={formData.content}/>
                    <Grid container spacing={2} alignItems="center">
                        {formData.formats.length > 0 && formData.formats.map((format, index) => (
                            <Grid item key={index}>
                                <BannerChipFormat format={format} color="primary"
                                                  handleClick={() => handleRemoveChip(index)}
                                />
                            </Grid>
                        ))}

                        {toggleFormatForm ? (
                            <FormatForm closeForm={handleFormatFormDisplay}
                                        handleFormSubmit={addFormatToState}/>
                        ) : (
                            <Grid item>
                                <IconButton size="small" color="primary" data-testid="toggleFormatForm"
                                            onClick={handleFormatFormDisplay}>
                                    <Add/>
                                </IconButton>
                            </Grid>
                        )}
                    </Grid>
                </Paper>
            </Grid>
            <Grid item xs={3}/>

            <Grid item xs={3}/>
            <Grid item xs={6} container spacing={3} justifyContent="center">
                <Grid item>
                    <Button color="primary" variant="contained" onClick={saveData} disabled={statusSave.isLoading}>
                        Sauvegarder
                    </Button>
                </Grid>
                <Grid item>
                    <Button variant="outlined" onClick={handleCancel}>Annuler</Button>
                </Grid>
                {handleDelete && (
                    <Grid item>
                        <Button variant="outlined" color="error" onClick={handleDelete}
                                disabled={statusDelete && statusDelete.isLoading}>Supprimer</Button>
                    </Grid>

                )}
            </Grid>
            {statusSave.isSuccess && (
                <AlertSnackbar open={statusSave.isSuccess} severity="success">
                    La bannière est sauvegardée.<br/>
                    <Button component={Link} to="/" size="small" variant="text" color="success">Revenir à la
                        liste</Button>
                </AlertSnackbar>
            )}
        </>
    );
};

export default Form;