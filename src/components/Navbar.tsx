import React from 'react';
import {AppBar, Box, Button, Container, Toolbar} from '@mui/material';
import {NavLink} from 'react-router-dom';

const Navbar = () => {
    return (
        <AppBar position="relative" color="primary">
            <Container maxWidth="lg" disableGutters>
                <Toolbar disableGutters>
                    <Box sx={{flexGrow: 1}}>
                        <NavLink to="/"><img src="/mediakeys.png" style={{height: '45px'}} alt="logo"/></NavLink>
                    </Box>
                    <Button component={NavLink} to="/create" variant="outlined" color="inherit">Créer une
                        bannière</Button>
                </Toolbar>
            </Container>
        </AppBar>
    );
};

export default Navbar;