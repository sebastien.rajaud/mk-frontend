import React, {ChangeEvent, useState} from 'react';
import {Button, Grid, IconButton, TextField} from '@mui/material';
import {Close} from '@mui/icons-material';
import {SubmitHandler, useForm} from 'react-hook-form';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup/dist/yup';

import {FormatInterface} from '../interfaces/BannerInterface';
import {FormatFormInterface} from '../interfaces/FormatFormInterface';

const FormatForm : React.FC<FormatFormInterface> = ({closeForm, handleFormSubmit}) => {

    const formatSchema = yup.object({
        width: yup.number().typeError('Veuillez fournir un nombre.')
            .required({message: 'Veuillez renseigner une largeur'}),
        height: yup.number().typeError('Veuillez fournir un nombre.')
            .required({message: 'Veuillez renseigner une hauteur'}),
    });

    const {register, handleSubmit, formState: {errors}, reset} = useForm<FormatInterface>({
        resolver: yupResolver(formatSchema),
    });
    const initialFormData = {
        width: undefined,
        height: undefined,
    };
    const [formatFormData, setFormatFormData] = useState<FormatInterface>(initialFormData);

    const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
        //handleFormDataChange(e.target);
        setFormatFormData((prevState) => ({
            ...prevState,
            [e.target.name]: e.target.value
        }));
    };
    const onSubmit: SubmitHandler<FormatInterface> = (data) => {
        handleFormSubmit(data);
        setFormatFormData(initialFormData);
        reset();
    };

    return (
        <Grid item xs={12}>
            <Grid container spacing={12}>
                <Grid item xs={10}>
                    <form className="mt-10" onSubmit={handleSubmit(onSubmit)}>
                        <Grid container alignItems="center" spacing={3}>
                            <Grid item xs={12} sm={5}>
                                <TextField {...register('width')} fullWidth label="Width"
                                           onChange={handleChange}
                                           size="small"
                                           value={formatFormData.width}
                                           error={!!errors.width?.message}
                                           helperText={errors.width?.message}
                                />
                            </Grid>
                            <Grid item xs={12} sm={5}>
                                <TextField {...register('height')} fullWidth label="Height"
                                           onChange={handleChange}
                                           size="small"
                                           value={formatFormData.height}
                                           error={!!errors.height?.message}
                                           helperText={errors.height?.message}
                                />
                            </Grid>
                            <Grid item xs={6} sm={2}>
                                <Button variant="contained" type="submit">Ajouter</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
                <Grid item xs={2}>
                    <IconButton size="small" color="primary" onClick={closeForm}>
                        <Close/>
                    </IconButton>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default FormatForm;