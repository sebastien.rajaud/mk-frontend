import {UserInterface} from './BannerInterface';

export interface BannerAvatarInterface {
    contributor: UserInterface
}