import {FormatInterface} from './BannerInterface';

export interface FormatFormInterface {
    closeForm: () => void,
    handleFormSubmit: (data: FormatInterface) => void
}