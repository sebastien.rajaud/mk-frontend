import {SelectChangeEvent} from '@mui/material';

import {SelectPropsInterface} from './BannerFiltersInterface';

export interface FilterInterface {
    inputLabel :string,
    label :string,
    onChange: (e:SelectChangeEvent) => void,
    value: string | undefined,
    values: Array<SelectPropsInterface>
}