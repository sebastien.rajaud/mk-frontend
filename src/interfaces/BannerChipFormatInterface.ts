import {FormatInterface} from './BannerInterface';

export interface BannerChipFormatInterface {
    format: FormatInterface,
    color?: 'default' | 'primary' | 'secondary' | 'error' | 'info' | 'success' | 'warning',
    handleClick ?: () => void,
}
