import {ReactNode} from 'react';

export interface AlertSnackbarInterface {
    severity: 'error' | 'warning' | 'success' | 'info',
    open: boolean,
    children: ReactNode
}