import {BannerInterface} from './BannerInterface';

export interface BannerListInterface {
    creatives: BannerInterface[]
}