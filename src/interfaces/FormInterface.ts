import {BannerInterface} from './BannerInterface';

interface StatusInterface {
    isLoading: boolean,
    isError: boolean,
    isSuccess: boolean,
}

export interface FormInterface {
    handleSave: (data: BannerInterface) => void,
    handleDelete?: () => void,
    initialValue: BannerInterface,
    statusDelete?: StatusInterface
    statusSave: StatusInterface,
    createMode?: boolean
}