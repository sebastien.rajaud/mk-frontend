import {BannerInterface} from './BannerInterface';

export interface BannerListItemInterface {
    creative: BannerInterface,
    divider: boolean
}