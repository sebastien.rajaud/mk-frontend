import {SelectChangeEvent} from '@mui/material';

export interface SelectPropsInterface {
    label: string,
    value: number | string,
}
export interface BannerFiltersInterface {
    order: string,
    handleOrder: (e: SelectChangeEvent) => void,
    limit: number,
    sort: string,
    handleLimit: (e: SelectChangeEvent) => void,
    handleSort: (e: SelectChangeEvent) => void,
    limitValues: Array<SelectPropsInterface>,
    sortValues: Array<SelectPropsInterface>
    orderValues: Array<SelectPropsInterface>
}