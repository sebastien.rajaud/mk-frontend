export interface UserInterface {
    id: string,
    firstName: string,
    lastName: string
}

export interface FormatInterface {
    width: number | undefined,
    height: number | undefined
}

export interface BannerInterface {
    id ?: string,
    createdBy : Omit<UserInterface, 'id'>,
    contributors: UserInterface[] | [],
    lastModified: string,
    enabled: boolean,
    title: string,
    description?: string,
    content?: string,
    formats: FormatInterface[] | [],
}