import {BannerInterface} from '../interfaces/BannerInterface';

const API_URL = 'http://localhost:3001';
export const getBannersWithPagination = async (page: number, limit: number, order = 'desc', sortBy: string) => {
    try {
        const response = await fetch(
            `${API_URL}/creatives?_page=${page}&_limit=${limit}&_sort=${sortBy}&_order=${order}`
        );
        if (response.ok) {
            return await response.json();
        } else {
            return Promise.reject('Une erreur est survenue lors de la requête');
        }
    } catch (e) {
        return Promise.reject('Une erreur est survenue.');
    }
};

export const getBanners = async () => {
    try {
        const response = await fetch(`${API_URL}/creatives`);
        if (response.ok) {
            return await response.json();
        } else {
            return Promise.reject('Une erreur est survenue lors de la requête');
        }
    } catch (e) {
        return Promise.reject('Une erreur est survenue.');
    }
};

export const getBannerDetail = async (id: string) => {
    try {
        const response = await fetch(`${API_URL}/creatives/${id}`);
        if (response.status === 404) {
            return Promise.reject('Aucun élément n\'a été trouvé avec cet identifiant');
        } else if (!response.ok) {
            return Promise.reject('Une erreur s\'est produite lors de la requête.');
        } else {
            return await response.json();
        }
    } catch (e) {
        return Promise.reject('Une erreur est survenue.');
    }
};

export const updateBanner = async (object: { id: string, banner: BannerInterface }) => {
    try {
        const response = await fetch(`${API_URL}/creatives/${object.id}`, {
            method: 'PUT',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(object.banner)
        });
        if (response.ok) {
            return await response.json();
        } else {
            return Promise.reject('Une erreur est survenue lors de la mise à jour.');
        }
    } catch (e) {
        return Promise.reject('Une erreur est survenue');
    }
};

export const deleteBanner = async (id: string) => {
    try {
        const response = await fetch(`${API_URL}/creatives/${id}`, {
            method: 'DELETE',
        });
        if (response.ok) {
            return await response.json();
        } else {
            return Promise.reject('Une erreur est survenue lors de la suppression.');
        }
    } catch (e) {
        return Promise.reject('Une erreur est survenue.');
    }
};

export const createBanner = async (data: BannerInterface) => {
    try {
        const response = await fetch(`${API_URL}/creatives/`, {
            method: 'POST',
            headers: {
                'content-type': 'application/json'
            },
            body: JSON.stringify(data)
        });

        if (response.ok) {
            return await response.json();
        } else {
            return Promise.reject('Une erreur est survenue lors de la création.');
        }
    } catch (e) {
        return Promise.reject('Une erreur est survenue.');
    }
};